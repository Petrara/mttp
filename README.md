# Android app - The secret life of food

<p float="middle">
  <img src="./app-images/photo_10@21-02-2021_18-59-50.jpg" width="100" />
  <img src="./app-images/photo_8@21-02-2021_18-59-50.jpg" width="100" /> 
  <img src="./app-images/photo_7@21-02-2021_18-59-50.jpg" width="100" />
  <img src="./app-images/photo_6@21-02-2021_18-59-50.jpg" width="100" />
  <img src="./app-images/photo_5@21-02-2021_18-59-50.jpg" width="100" /> 
  <img src="./app-images/photo_4@21-02-2021_18-59-50.jpg" width="100" />
  <img src="./app-images/photo_3@21-02-2021_18-59-50.jpg" width="100" />
  <img src="./app-images/photo_2@21-02-2021_18-59-50.jpg" width="100" /> 
</p>

More images can be found in the [app-images](https://gitlab.com/Petrara/mttp/-/tree/develop-test/app-images) folder.

**Automated** testing was performed using _Android SDK(Android Studio)_, _JDK_, _IDE - IntelliJ_, **Test Framework - TestNG**, **Selenium** and **Appium**. **Manual testing** was also performed on the application.

Android application **The secret life of food** is the result of my own development for the needs of the course _Basics of web and mobile application development_ at The Faculty of Electrical Engineering, Computer Science and Information Technology Osijek.

It is written in the **Java** programming language, and offers an overview of different types of dishes and recipes that are available in their original form and through the Youtube channel.
The selection of favorite dishes can be done by pressing the heart icon next to each dish, and they are stored in a local database using **Room**.

The app is available in the [the-secret-life-of-food](https://gitlab.com/Petrara/the-secret-life-of-food) repository on my GitLab.

# Installation
1. Install [JDK](https://www.guru99.com/install-java.html) (Java Development Kit)

2. Install [Android SDK](https://developer.android.com/studio) (Android Studio)

3. Create and/or run an Android emulator from Android Studio:

`Tools > AVD Manager > Run`


4. Navigate to `C:\Users\User\AppData\Local\Android\Sdk\platform-tools` using cmd and write the instruction below:

`adb start-server`

5. Check if you have Node.js platform installed with `node --version`. If not, [download](https://nodejs.org/en/download/) and install it.

6. Download [Appium](https://github.com/appium/appium-desktop/releases/tag/v1.18.3) (depending on your operating system), unzip it and start. After that, install [Selenium-Java](https://mvnrepository.com/artifact/org.seleniumhq.selenium/selenium-java).

7. Edit configurations for **ANDROID_HOME** and **JAVA_HOME** system variables.

8. Create New session and update the following settings with your own:
```
{
  "app": "D:\\User\\MTTPP\\TheSecretLifeOfFood.apk",
  "VERSION": "9.0",
  "deviceName": "emulator",
  "platformName": "Android"
}
```

* use this settings in your Maven project respectively

9. Clone [the repo](https://gitlab.com/Petrara/mttp) and find [Android app](https://gitlab.com/Petrara/the-secret-life-of-food).

10. Open [IntelliJ](https://www.jetbrains.com/idea/download/#section=windows) and create a new Maven project with Appium and [TestNG](https://mvnrepository.com/artifact/org.testng/testng) dependency in pom.xml

11. Finally, start tests with:

`mvn test (CTRL + Enter)`

_Optional: generate a report_

# Page Object Model (POM)
This framework uses the Page Object Model (POM) design pattern. It is a design pattern that allows better performance and reduces duplicate code.


<p float="middle">
  <img src="./app-images/POM_example.png" width="800" height="500" />
</p>

A page is an object-oriented class that serves as an interface to a page of your application under test. In this class, you will store ids, actions, and validators specific to the UI elements from a page.

The benefits of POM are:

- Reusability: the page class can be reused if required in different test cases.

- Maintainability: the code can be easily updated if any new UI element is added or an existing one is updated.

- Readability: code is separated from test code which helps to improve code readability.

# Available tests

All of the tests are located within the src/test directory.

Due to SplashScreenActivity which is LAUNCH activity before the main screen, the following instruction is set:

`driver.manage (). timeouts (). implicitlyWait (20, TimeUnit.SECONDS);`

| Class name | Description |
| ------ | ------ |
| [ViewPagerTest](https://gitlab.com/Petrara/mttp/-/blob/develop-test/MTTP-test/src/test/java/ViewPagerTest.java) | tests the correct display of recipe of the day by pressing the view pager on the main activity of the application |
| [RecipeDescriptionTest](https://gitlab.com/Petrara/mttp/-/blob/develop-test/MTTP-test/src/test/java/RecipeDescriptionTest.java) | checks the display of meal descriptions and required ingredients |
| [SourceRecipeTest](https://gitlab.com/Petrara/mttp/-/blob/develop-test/MTTP-test/src/test/java/SourceRecipeTest.java) | goes to the original recipe which is available in written from |
| [YoutubeRecipeTest](https://gitlab.com/Petrara/mttp/-/blob/develop-test/MTTP-test/src/test/java/YoutubeRecipeTest.java) | goes to the original recipe which is available on the Youtube channel |
| [SaveInRoomTest](https://gitlab.com/Petrara/mttp/-/blob/develop-test/MTTP-test/src/test/java/SaveInRoomTest.java) | in the selection of meal categories (desserts), the meal is selected and the storage in the Room is tested by pressing on the heart icon |
| [NavigationTest](https://gitlab.com/Petrara/mttp/-/blob/develop-test/MTTP-test/src/test/java/NavigationTest.java) | tests in-app navigation between view pager, meal categories and recipes |

- the tests are chained

- scroll is enabled

# Additional information
Auto DevOps (CI/CD) is enabled.

Gitignore includes Java, IntelliJ and Android Studio.

# License
Licensed under the MIT license. For more information see [LICENSE](https://gitlab.com/Petrara/mttp/-/blob/master/LICENSE).
