import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class RecipeDescriptionTest implements Test {

    private AndroidDriver<AndroidElement> driver;

    public RecipeDescriptionTest() {
    }

    public RecipeDescriptionTest(AndroidDriver<AndroidElement> driver) {
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Beef']")
    private AndroidElement beefMeal;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Beef Bourguignon']")
    private AndroidElement bourguignonDish;

    @Override
    public void test() {
        beefMeal.click();
        bourguignonDish.click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.navigate().back();
        driver.navigate().back();
    }
}