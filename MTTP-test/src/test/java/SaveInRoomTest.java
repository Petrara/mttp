import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class SaveInRoomTest implements Test {

    private AndroidDriver<AndroidElement> driver;

    public SaveInRoomTest() {
    }

    public SaveInRoomTest(AndroidDriver<AndroidElement> driver) {
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='BEEF']")
    private AndroidElement beefMeal;

    @AndroidFindBy(id = "com.example.android_projekt:id/heart_checker")
    private AndroidElement heart;

    @Override
    public void test() {
        beefMeal.click();
        heart.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }
}