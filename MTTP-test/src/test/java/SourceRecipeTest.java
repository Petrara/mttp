import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class SourceRecipeTest implements Test {

    private AndroidDriver<AndroidElement> driver;

    public SourceRecipeTest() {
    }

    public SourceRecipeTest(AndroidDriver<AndroidElement> driver) {
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Dessert']")
    private AndroidElement dessertMeal;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Apple Frangipan Tart']")
    private AndroidElement tartMeal;

    public void scrollAndClick(String text) {
        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+text+"\").instance(0))").click();
    }

    @Override
    public void test() {
        dessertMeal.click();
        tartMeal.click();
        scrollAndClick("Source");
        driver.navigate().back();
    }
}