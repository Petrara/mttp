import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

public class YoutubeRecipeTest implements Test {

    private AndroidDriver<AndroidElement> driver;

    public YoutubeRecipeTest() {
    }

    public YoutubeRecipeTest(AndroidDriver<AndroidElement> driver) {
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='LAMB']")
    private AndroidElement lambMeal;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Kapsalon']")
    private AndroidElement kapsalonDish;

    public void scrollAndClick(String text) {
        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+text+"\").instance(0))").click();
    }

    @Override
    public void test() {
        lambMeal.click();
        kapsalonDish.click();
        scrollAndClick("Youtube");
    }
}