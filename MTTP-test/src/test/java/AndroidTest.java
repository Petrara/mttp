import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeUnit;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.Activity;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class AndroidTest {

    private final static String APP_PATH = "C:\\Users\\Radocaj\\AndroidStudioProjects\\the-secret-life-of-food\\app\\build\\outputs\\apk\\debug\\app-debug.apk";
    private final static String APP_VERSION = "9.0";
    private final static String APP_DEVICE_NAME = "emulator";
    private final static String APP_PLATFORM_NAME = "Android";
    private final static String ANDROID_DRIVER_URL = "http://127.0.0.1:4723/wd/hub";

    AndroidDriver driver;

    @BeforeClass()
    public void setUp() throws MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("app", APP_PATH);
        capabilities.setCapability("VERSION", APP_VERSION);
        capabilities.setCapability("deviceName", APP_DEVICE_NAME);
        capabilities.setCapability("platformName", APP_PLATFORM_NAME);
        driver = new AndroidDriver(new URL(ANDROID_DRIVER_URL), capabilities);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    @Test
    public void testCal() throws Exception {
        ViewPagerTest viewPagerTest = new ViewPagerTest(driver);
        RecipeDescriptionTest recipeDescriptionTest = new RecipeDescriptionTest(driver);
        SourceRecipeTest sourceRecipeTest = new SourceRecipeTest(driver);
        YoutubeRecipeTest youtubeRecipeTest = new YoutubeRecipeTest(driver);
        SaveInRoomTest saveInRoomTest = new SaveInRoomTest(driver);
        NavigationTest navigationTest = new NavigationTest(driver);

        viewPagerTest.test();
        recipeDescriptionTest.test();
        sourceRecipeTest.test();
        navigationTest.test();
        saveInRoomTest.test();
        youtubeRecipeTest.test();
        teardown();
    }

    public void teardown(){
        driver.quit();
    }
}