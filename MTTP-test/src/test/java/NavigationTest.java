import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

public class NavigationTest implements Test {

    private AndroidDriver<AndroidElement> driver;

    public NavigationTest() {
    }

    public NavigationTest(AndroidDriver<AndroidElement> driver) {
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Beef']")
    private AndroidElement beefMeal;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='LAMB']")
    private AndroidElement lambMeal;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='CHICKEN']")
    private AndroidElement chickenMeal;

    @Override
    public void test() {
        beefMeal.click();
        lambMeal.click();
        chickenMeal.click();
    }
}